# Input Datasets

## Core ∆∆G dataset

- `khan_protherm_data_mapped.xlsx` - Dataset 1 on the [VariBench protein stability webiste](http://structure.bmc.lu.se/VariBench/).
- `strum_q3421.tsv` - Q3421 dataset from the [STRUM paper](https://doi.org/10.1093/bioinformatics/btw361).

# Interface ∆∆G dataset

- `ab-bind` - Cloned from https://github.com/sarahsirin/AB-Bind-Database.
- `skempi-v2` - https://life.bsc.es/pid/skempi2/.
 
