# tkpod

[![docs](https://img.shields.io/badge/docs-v0.1-blue.svg)](https://datapkg.gitlab.io/tkpod/)
[![build status](https://gitlab.com/datapkg/tkpod/badges/master/build.svg)](https://gitlab.com/datapkg/tkpod/commits/master/)

Benchmark suite for tkpod plugins.

## Notebooks
